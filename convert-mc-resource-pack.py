#!/usr/bin/python3

import shutil, zipfile, sys, getopt, os, json, platform, csv, posixpath, subprocess, math

pack_format=1
TEXTURE_SIZE = 64
#Turn this to true to have files inserted into the MCL2 mod directory to use as default textures
generating_for_game=False

#This is the path to the zipped source resource pack
input_zip=""

#This is the path to the ouput directory mcl2 pack
output_directory="CONVERTED_TEXTURE_PACK"

#This is the path to the temporary unzipped source resource pack
input_directory="UNZIPPED_TMP_FILE_ABCDEFGHJKILM"

#"Linux" is linux, "Windows" is windows, "Darwin" is MacOS/Apple
platform=platform.system()

#Image Magick command variables
magick_prefix = ""
CONVERT="convert"
COMPOSITE="composite"
MOGRIFY="mogrify"
DISPLAY="display"
ANIMATE="animate"
COMPARE="compare"
CONJURE="conjure"
IDENTIFY="identify"
IMPORT="import"
MONTAGE="montage"
STREAM="stream"
magick_version = 7
VERSION="1.0"

warning_count=0
LOG_FILE="converter.log"
WARN_FILE="converter.warn.log"
warnLogFile = ""
logFile = ""

# Set to true to enable logging of warnings
debug = False
if debug:
    warnLogFile = open(WARN_FILE, 'w')
    logFile = open(LOG_FILE, 'w')

def warn(msg):
    global warning_count, debug
    print("WARNING: " + msg)
    warning_count += 1
    if debug:
	    warnLogFile.write("WARNING: " + msg + "\n")

def log(msg):
    if debug:
        logFile.write(msg + "\n")

# This function is called to check if an image is animated. Returns True if it is animated, false if it is a static image
def is_animated(source):
    global TEXTURE_SIZE
    mcmeta_source = source + ".mcmeta"
    if os.path.isfile(mcmeta_source):
        with open(mcmeta_source) as block_meta:
            block_info = json.load(block_meta)
            if "animation" in block_info:
                return True
    return False
	
def crop_image(xs,ys,xl,yl,xt,yt,src,dst):
    global magick_prefix, CONVERT, COMPOSITE, MOGRIFY, DISPLAY, ANIMATE, COMPARE, CONJURE, IDENTIFY, IMPORT, MONTAGE, STREAM
    if magick_version == 7:
        try:
            subprocess.run([magick_prefix, CONVERT, src, "-crop", str(xl) + "x" + str(yl) + "+" + str(xs) + "+" + str(ys), str(dst)], check=True, shell=True)
        except subprocess.CalledProcessError as err:
            warn("Crop of source file " + str(src) + " failed! Skipping...")
            print(err)
            return False
        return True

# Called to colorize a greyscale texture
def colorize_greyscale(src, colorMap, selectedColormapPixel, textureSize, dst):
    global magick_prefix, CONVERT, COMPOSITE, MOGRIFY, DISPLAY, ANIMATE, COMPARE, CONJURE, IDENTIFY, IMPORT, MONTAGE, STREAM
    if magick_version == 7:
        try:
            subprocess.run([magick_prefix, CONVERT, colorMap, "-crop", "1x1+"+selectedColormapPixel, "-depth", "8", "-resize", str(textureSize) + "x" + str(textureSize), "tmpABCDEFGH1234567File.png"], check=True, shell=True)
        except subprocess.CalledProcessError as err:
            warn("Convert of greyscale of source file " + str(src) + " failed! Skipping...")
            print(err)
    
        try:
            subprocess.run([magick_prefix, COMPOSITE, "-compose", "Multiply", "tmpABCDEFGH1234567File.png", src, dst], check=True, shell=True)
        except subprocess.CalledProcessError as err:
            warn("Composite of greyscale of source file " + str(src) + " failed! Skipping...")
            print(err)
            return False
        return True

# Called to colorize a greyscale texture and preserve alpha
def colorize_alpha(src, colorMap, selectedColormapPixel, textureSize, dst):
    global magick_prefix, CONVERT, COMPOSITE, MOGRIFY, DISPLAY, ANIMATE, COMPARE, CONJURE, IDENTIFY, IMPORT, MONTAGE, STREAM
    if not colorize_greyscale(src, colorMap, selectedColormapPixel, textureSize, "tmpBCDEFGHI123456.png"):
        return False
    if magick_version == 7:
        try:
            subprocess.run([magick_prefix, COMPOSITE, "-compose", "Dst_In", src, "tmpBCDEFGHI123456.png", "-alpha", "Set", str(dst)], check=True, shell=True)
        except subprocess.CalledProcessError as err:
            warn("Colorizing alpha of source file " + str(src) + " failed! Skipping...")
            print(err)
            return False
        return True

#This function checks to ensure that Image Magick is installed, and if it is, detects if the programs are installed separately or as one
def check_magick():
    global magick_prefix, CONVERT, COMPOSITE, MOGRIFY, DISPLAY, ANIMATE, COMPARE, CONJURE, IDENTIFY, IMPORT, MONTAGE, STREAM
    if shutil.which("magick") == None:
        if shutil.which("compose") == None:
            return False
        else:
            magick_prefix = ""
            log("No prefix for magick")
            return True
    else:
        log("Using magick prefix for magick")
        magick_prefix = "magick"
        #CONVERT="magick convert"
        #COMPOSITE="magick composite"
        #MOGRIFY="magick mogrify"
        #DISPLAY="magick display"
        #ANIMATE="magick animate"
        #COMPARE="magick compare"
        #CONJURE="magick conjure"
        #IDENTIFY="magick identify"
        #IMPORT="magick import"
        #MONTAGE="magick montage"
        #STREAM="magick stream"
        return True

#Prints usage text
def show_usage():
    print("Texture Pack Converter")
    print("Version: " + VERSION)
    print("This script is designed to take a MC (1.14) resource pack and create a texture pack for use with MineClone2.")
    print("DO NOT REDISTRIBUTE ANY RESULUTING TEXTUREPACKS! COPYRIGHT AND LICENSING RESTRICTIONS MOST LIKELY STILL APPLY SO DO NOT REDISTRIBUTE ANY RESULTING TEXTURE PACKS!")
    print("")
    print("Usage:")
    print("	python3 convert-mc-resource-pack.py [OPTIONS] INPUT-RESOURCE-PACK.zip [OUTPUT-NAME]")
    print("")
    print("		INPUT-RESOURCE-PACK does not have to be in a .zip file. If it is unzipped already then that is ok. If it is zipped, this script will unzip it into this directory.")
    print("		OUTPUT-NAME is optional, if no output name is given, the resulting MineClone2 texture pack will be written to OUTPUT-NAME, or set to the value of .pack.name in the pack.mcmeta file.")
    print("		OPTIONS may be one of:")
    print("			-h --help	Display this usage text.")
    print("			-v --version	Display the version of this script and then exit.")
    print("			-d --debug	Enables debug mode and logging to log and warning files.")

#Prints version message
def show_version():
    print("Cross Platform Minecraft Resource Pack Converter Version " + VERSION)

#Processes cmdln args, exits after printing usage or version text. Handles errors for too many or too few args.
def process_args():
    global input_zip, output_directory, logFile, warnLogFile, debug
    try:
        opts, args = getopt.gnu_getopt(sys.argv[1:], "hvd", ["help", "version", "debug"])
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit(2)
    for o, a in opts:
        if o in ("-h", "--help"):
              show_usage()
              sys.exit(0)
        elif o in ("-v", "--version"):
              show_version()
              sys.exit(0)
        elif o in ("-d", "--debug"):
              debug = True
              warnLogFile = open(WARN_FILE, 'w')
              logFile = open(LOG_FILE, 'w')
    if len(args) < 1:
        print("No input file specified! You must specify the path to the resource pack you want to convert! Stopping.")
        sys.exit(-1)
    else:
        input_zip = args[0]
        if len(args) > 1:
            if len(args) == 2:
                output_directory=args[1]
            else:
                print("Too many arguments specified! Please see --help for correct usage!")
                sys.exit(-1)
        else:
            output_directory=os.path.join(os.path.dirname(input_zip), os.path.splitext(os.path.basename(input_zip))[0] + "_CONVERTED", "")

#Ensures that the input file is a ZIP, checks that it exists, and extracts it to the temporary input_directory
def extract_zip():
    global input_zip, output_path, platform, input_directory
    if not ".zip" == os.path.splitext(input_zip)[-1]:
        print("Error! Selected input file is not a ZIP file!")
        sys.exit(-1)
    if not os.path.isfile(input_zip):
        print("Error! Selected input file does not exist!")
        sys.exit(-1)
    print("Attempting to extract targeted ZIP file...")
    input_directory = os.path.join(os.path.dirname(input_zip), os.path.splitext(os.path.basename(input_zip))[0] + "_UNZIPPED", "")
    with zipfile.ZipFile(input_zip, 'r') as zip_ref:
        zip_ref.extractall(input_directory)

#Ensures that the unzipped resource pack looks like a resource pack and extracts the version information from it
def check_structure():
    global input_directory, platform, output_directory, pack_format
    print("Ensuring extracted resource pack is sane...")
    if not os.path.isdir(input_directory):
        print("Extraction failed! Double check your file permissions!")
        sys.exit(-1)
    if not os.path.isdir(os.path.join(input_directory, "assets", "")):
        print("Extracted ZIP does not contain an assets directory! Are you sure it's a resource pack?")
        sys.exit(-1)
    if os.path.isfile(os.path.join(input_directory, "pack.mcmeta")):
        print("Found MCMETA file! Yay! Parsing information...")
        log("Found MCMETA file")
        with open(os.path.join(input_directory, "pack.mcmeta")) as mcmeta:
            pack_info = json.load(mcmeta)
            pack_format = pack_info["pack"]["pack_format"]
            print("Resource pack is in pack format " + str(pack_format))
            log("Resource pack is in pack format " + str(pack_format))
    else:
        warn("No MCMETA file found! Assuming old version 1 format...")
        pack_format = 1

#Creates the output directory
def create_output_directory():
    global output_directory, platform
    if os.path.isdir(output_directory):
        warn("Output directory " + output_directory + " already exists, overwriting!")
        shutil.rmtree(output_directory)
    os.mkdir(output_directory)
    log("Created output directory")

#Ensures that we have access to the json reference files we use to convert files
def check_json_files():
    global pack_format
    if not os.path.isdir("conversion-data"): #We are boldly assuming that if this directory exists then all of the necessary subdirs and files exist as well
        print("ERROR! FATAL! The conversion-data directory could not be found! This directory is critical for this program to work!")
        sys.exit(-1)

#Deletes all temporary files and directories.
def clean_up():
    global input_directory, output_directory
    #shutil.rmtree(input_directory)

def convert_block_break():
    global input_directory, output_directory, magick_prefix, CONVERT, COMPOSITE, MOGRIFY, DISPLAY, ANIMATE, COMPARE, CONJURE, IDENTIFY, IMPORT, MONTAGE, STREAM
    log("Converting block break animation...")
    destination = os.path.join(output_directory, "crack_anylength.png")            
    if pack_format == 4:
        base = "/assets/minecraft/textures/block/destroy_stage_"
    else:
        base = "/assets/minecraft/textures/blocks/destroy_stage_"
    if not os.path.isfile(os.path.join(input_directory, base[1:].replace("/",os.sep)+"0.png")):
        warn("No block breaking animation found, skipping...")
        return False
    else:
        done = False
        i=0
        source = os.path.join(input_directory, base[1:].replace("/",os.sep)+str(i)+".png")
        if os.path.isfile(destination):
            os.remove(destination) # delete old conversion so shutil doesn't error out while copying
        shutil.copy(source,destination)
        i=1
        while not done: 
            source = os.path.join(input_directory, base[1:].replace("/",os.sep)+str(i)+".png")
            if not os.path.isfile(source):
                done = True
                break
            if os.path.isfile("tmpABCD.png"):
                os.remove("tmpABCD.png")
                
            try:
                subprocess.run([magick_prefix, MONTAGE, "-tile", "1x" + str(i), "-geometry", "+0+0", "-background", "none", str(source), str(destination), destination], check=True, shell=True)
            except subprocess.CalledProcessError as err:
                warn("Montage for crackig animation failed! Skipping...")
                print(err)
                return False
            
            try:
                subprocess.run([magick_prefix, CONVERT, destination, "-alpha", "on", "-background", "none", "-channel", "A", "-evaluate", "Min", "50%", str(destination)], check=True, shell=True)
            except subprocess.CalledProcessError as err:
                warn("Convert for crackig animation failed! Skipping...")
                print(err)
                return False
            i+=1
        try:
            subprocess.run([magick_prefix, CONVERT, destination, "-rotate", "180", destination], check=True, shell=True)
        except subprocess.CalledProcessError as err:
            warn("Rotation for crackig animation failed! Skipping...")
            print(err)
            return False
    return True

#Called to receive MCMETA info on a texture if it exists. Returns None if MCMETA doesn't exist
def get_texture_mcmeta(source):
    mcmeta_source = source + ".mcmeta"
    if os.path.isfile(mcmeta_source):
        with open(mcmeta_source) as block_meta:
            meta_info = json.load(block_meta)
            return meta_info
    return None

#Called when a conversion target may have alternative source file names and needs resolved. Returns None if source file could not be resolved.
def resolve_source_file(source_directory, source_file, alternatives):
    global input_directory
    source = os.path.join(source_directory[1:].replace("/",os.sep), source_file)
    source = os.path.join(input_directory, source)
                
    if os.path.isfile(source):
        return source
    for a in alternatives:
        source = os.path.join(source_directory[1:].replace("/",os.sep), a)
        source = os.path.join(input_directory, source)
        if os.path.isfile(source):
            return source
    return None

#Called to use conversion data from all JSON conversion files in a directory
def convert_directory(json_directory):
    global input_directory, output_directory, generating_for_game, pack_format, TEXTURE_SIZE
    for json_file in os.listdir(json_directory):
        if not json_file.endswith(".json"):
            continue #File isn't a JSON file so skip it b/c it doesnt have any info for usage
        
        with open(os.path.join(json_directory, json_file), 'r') as jf: 
            try:
                target_info = json.load(jf) # target_info is current conversion target
            except ValueError as e:
                print('invalid json: %s' % e)
                warn("Invalid JSON file " + json_file + ": %s" % e)
                continue
                
            #Sanity checks because python errors are UGLY and useless
            log("Iterating over " + json_file)
            if not "target_file" in target_info:
                warn("target_file key not found for JSON file " + json_file + ", skipping!")
                continue
            if not "formats" in target_info:
                warn("formats key not found for JSON file " + json_file + ", skipping!")
                continue
                
            # Resolve the destination
            target_file = target_info["target_file"]
            if generating_for_game:
                target_directory = output_directory, target_info["target_directory"]
                destination = os.path.join(target_directory, target_file)
            else:
                destination = os.path.join(output_directory, target_file)
            
            # Read in the default conversion parameters and then overlay the pack format specific options
            if "default" in target_info["formats"]:
                conversion = target_info["formats"]["default"]
                if not str(pack_format) in target_info["formats"]:
                    log("No specific conversion info found for conversion target " + json_file + ", assuming default parameters...")
                else:
                    conversion.update(target_info["formats"][str(pack_format)])
            else:
                if not str(pack_format) in target_info["formats"]:
                    log("No specific conversion info, or defaults found for conversion target " + json_file + ", skipping!")
                    continue
                else:
                    conversion = target_info["formats"][str(pack_format)]
            
            #Make sure we know what type of conversion we are doing...
            if not "type" in conversion:
                warn("No type key found in conversion data for " + json_file + ", assuming copy...")
                conversion["type"] = "copy"
                
            #This is important for defaults
            if not "is_animated" in conversion:
                log("No is_anmiated key found in conversion data for " + json_file + ", assuming false...")
                conversion["is_animated"] = False
                
            conversion_type = conversion["type"]
            if conversion_type == "copy":
                if not "source_file" in conversion or not "source_directory" in conversion: # sanity check
                    warn("source_file or source_directory keys not present in conversion data for conversion target " + json_file + ", skipping!")
                    continue
                source_file = conversion["source_file"]
                source_directory = conversion["source_directory"]
                source = os.path.join(source_directory[1:].replace("/",os.sep), source_file)
                source = os.path.join(input_directory, source)
                
                if "alternative_source_files" in conversion:
                    source = resolve_source_file(source_directory, source_file, conversion["alternative_source_files"])
                    if source == None:
                        warn("Source file and alternatives were not found for conversion target " + json_file + ", skipping!")
                        continue
                elif not os.path.isfile(source):
                    warn("Source file " + source_file + " was not found for conversion target " + json_file + ", skipping!")
                    continue
                
                mcmeta = get_texture_mcmeta(source)
                
                if not mcmeta == None:
                    if not conversion["is_animated"] and "animation" in mcmeta: #MCL2 file should not be animated, but the source resource pack is animated. 
                        warn("conversion target " + json_file + " is not animated, but source resource pack does have it animated. Cropping a single frame...")
                        crop_image(0,0,TEXTURE_SIZE,TEXTURE_SIZE,0,0,source,destination)
                else:
                    shutil.copy(source, destination)
                
            elif conversion_type == "crop":
                if not "source_file" in conversion or not "source_directory" in conversion: # sanity check
                    warn("source_file or source_directory keys not present in conversion data for conversion target " + json_file + ", skipping!")
                    continue
                if not "xs" in conversion or not "ys" in conversion or not "xl" in conversion or not "yl" in conversion: # sanity check
                    warn("one of the xs, ys, xl, yl keys not present in conversion data for cropped conversion target " + json_file + ", skipping!")
                    continue
                    
                source_file = conversion["source_file"]
                source_directory = conversion["source_directory"]
                source = os.path.join(source_directory[1:].replace("/",os.sep), source_file)
                source = os.path.join(input_directory, source)
                
                if "alternative_source_files" in conversion:
                    source = resolve_source_file(source_directory, source_file, conversion["alternative_source_files"])
                    if source == None:
                        warn("Source file and alternatives were not found for conversion target " + json_file + ", skipping!")
                elif not os.path.isfile(source):
                    warn("Source file " + source_file + " was not found for conversion target " + json_file + ", skipping!")
                    continue
                
                if not "xt" in conversion:
                    xt = 0
                else:
                    xt = conversion["xt"]
                    
                if not "yt" in conversion:
                    yt = 0
                else:
                    yt = conversion["yt"]
                    
                xs = conversion["xs"]
                ys = conversion["ys"]
                xl = conversion["xl"]
                yl = conversion["yl"]
                
                log("Cropping source file " + source_file + " for target " + json_file)
                crop_image(xs,ys,xl,yl,xt,yt,source,destination)
                
            elif conversion_type == "colorize":
                if not "source_file" in conversion or not "source_directory" in conversion: # sanity check
                    warn("source_file or source_directory keys not present in conversion data for conversion target " + json_file + ", skipping!")
                    continue
                if not "colormap_name" in conversion or not "colormap_point" in conversion or not "colormap_directory" in conversion: # sanity check
                    warn("colormap_name or colormap_directory or colormap_point keys not present in conversion data for colorize conversion target " + json_file + ", skipping!")
                    continue
                    
                source_file = conversion["source_file"]
                source_directory = conversion["source_directory"]
                source = os.path.join(source_directory[1:].replace("/",os.sep), source_file)
                source = os.path.join(input_directory, source)
                
                if "alternative_source_files" in conversion:
                    source = resolve_source_file(source_directory, source_file, conversion["alternative_source_files"])
                    if source == None:
                        warn("Greyscale source file and alternatives were not found for conversion target " + json_file + ", skipping!")
                elif not os.path.isfile(source):
                    warn("Greyscale source file " + source_file + " was not found for conversion target " + json_file + ", skipping!")
                    continue
                    
                colormap = os.path.join(input_directory, conversion["colormap_directory"][1:].replace("/",os.sep), conversion["colormap_name"])
                if not os.path.isfile(colormap):
                    warn("Colormap " + colormap + " was not found for colorize conversion target " + json_file + ", skipping!")
                    continue
                    
                log("Colorizing greyscale source file " + source_file)
                colorize_alpha(source, colormap, conversion["colormap_point"], TEXTURE_SIZE, destination)
                
            elif conversion_type == "clock":
                if not "source_file_prefix" in conversion or not "source_directory" in conversion: # sanity check
                    warn("source_file_prefix or source_directory keys not present in conversion data for clock conversion target " + json_file + ", skipping!")
                    continue
                    
                source_directory = conversion["source_directory"]
                source_prefix = conversion["source_file_prefix"]
                    
                if not "frames_needed" in conversion:
                    frames_needed = 24
                else:
                    frames_needed = int(conversion["frames_needed"])
                    
                if not os.path.isfile(os.path.join(input_directory, source_directory[1:].replace("/",os.sep), source_prefix + "00.png")):
                    warn("Frame 00 for clock conversion target " + json_file + " not found! Skipping...")
                    continue
                    
                target_file_prefix = target_file.split(".")[0] + "_"
                
                done = False
                i=0
                while not done: #Iterate until we reach the last frame of the clock animation
                    source_file = source_prefix + "{:02d}".format(i) + ".png"
                    source_directory = conversion["source_directory"]
                    source = os.path.join(source_directory[1:].replace("/",os.sep), source_file)
                    source = os.path.join(input_directory, source)
                    
                    if not os.path.isfile(source):
                        done = True
                        break
                    i += 1
                    
                step = math.floor(i / frames_needed)
                
                input_frame_index = 0
                output_frame_index = 0
                while not frames_needed == output_frame_index:
                    # Resolve the destination
                    target_file = target_file_prefix + "{:02d}".format(output_frame_index) + ".png"
                    if generating_for_game:
                        target_directory = output_directory, target_info["target_directory"]
                        destination = os.path.join(target_directory, target_file)
                    else:
                        destination = os.path.join(output_directory, target_file)
                        
                    source_file = source_prefix + "{:02d}".format(input_frame_index) + ".png"
                    source_directory = conversion["source_directory"]
                    source = os.path.join(source_directory[1:].replace("/",os.sep), source_file)
                    source = os.path.join(input_directory, source)
                    
                    if not os.path.isfile(source):
                        warn("Frame " + "{:02d}".format(frame_index) + " not found for clock conversion target " + json_file + "! Skipping!")
                        input_frame_index += step
                        output_frame_index += 1
                        continue
                    
                    shutil.copy(source, destination)
                    input_frame_index += step
                    output_frame_index += 1

process_args()
check_magick()

#User can specify to input an already extracted ZIP file instead of extracting all over again
if not os.path.isdir(input_zip):
    extract_zip()
    if not generating_for_game:
        create_output_directory()
else:
    input_directory = input_zip
    if os.path.isdir(input_zip.split("_")[0] + "_CONVERTED") and not generating_for_game:
        os.rmtree(input_zip.split("_")[0] + "_CONVERTED")
        os.mkdir(input_zip.split("_")[0] + "_CONVERTED")
    if not os.path.isdir(input_zip.split("_")[0] + "_CONVERTED") and not generating_for_game:
        create_output_directory()

check_structure()
check_json_files()

print("Converting blocks...")
convert_directory(os.path.join("conversion-data", "blocks"))

print("Converting items...")
convert_directory(os.path.join("conversion-data", "items"))

print("Converting entities...")
convert_directory(os.path.join("conversion-data", "entities"))

print("Converting block break animation...")
convert_block_break()

clean_up()
log("Conversion Finished.")
if debug:
    warnLogFile.close()
    logFile.close()

print("Finished with " + str(warning_count) + " warnings.")
