# Better MCL2 Converter
This is a second iteration at attempts to create the best Minecraft to Mine Clone 2 texture pack converter.  
This script breaks apart each outputted image into individual JSON files known as "conversion targets".  
The script iterates through the conversion targets in the `conversion-data` directory to find the instructions on how to convert each image for MCL2.

## Features
* Cross platform (windows and linux)
* Converts clock items
* Converts grass and foliage
* Handles cases where input resource pack has animated textures that are not animated in MCL2

## Requirements
Image magick 6 or 7 need to be installed. Python 3 is required. 

## Usage
    python3 convert-mc-resource-pack.py [OPTIONS] INPUT-RESOURCE-PACK.zip [OUTPUT-NAME]
    
        INPUT-RESOURCE-PACK does not have to be in a .zip file. If it is unzipped already then that is ok. If it is zipped, this script will unzip it into this directory.
        OUTPUT-NAME is optional, if no output name is given, the resulting MineClone2 texture pack will be written to OUTPUT-NAME, or set to the value of .pack.name in the pack.mcmeta file.
        OPTIONS may be one of:
            -h --help	Display this usage text.
            -v --version	Display the version of this script and then exit.
            -d --debug	Enables debug mode and logging to log and warning files.
            
## Roadmap
1. Convert compasses
2. Allow for user specification of base tile size
3. Convert normal and specular maps of blocks
4. Add "sequence" based conversion type
5. Add "UV remapping" based conversion type
5. Convert single chests
6. Convert ender chests
7. Convert double chests
8. Convert armor
9. Documentation